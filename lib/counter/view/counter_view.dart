import 'package:bloc_lecture_practice/counter/cubit/counter_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounterView extends StatelessWidget {
  const CounterView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Counter')),
      body: Center(
        child: BlocBuilder<CounterCubit, Data>(
          builder: (context, state) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('int a: ${state.i}', style: TextStyle(fontSize: 40.0)),
                const Divider(thickness: 5),
                Text('int b: ${state.b}', style: TextStyle(fontSize: 40.0)),
              ],
            );
          },
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
              icon: Icon(Icons.add),
              iconSize: 30,
              onPressed: () {
                return context.read<CounterCubit>().increment();
              }),
          IconButton(
              icon: Icon(Icons.remove),
              iconSize: 30,
              onPressed: () {
                return context.read<CounterCubit>().decrement();
              }),
          IconButton(
              icon: Icon(Icons.exposure_zero),
              iconSize: 30,
              onPressed: () {
                return context.read<CounterCubit>().zero();
              }),
        ],
      ),
    );
  }
}
