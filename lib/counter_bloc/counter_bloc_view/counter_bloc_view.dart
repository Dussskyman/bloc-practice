import 'package:bloc_lecture_practice/counter/cubit/counter_cubit.dart';
import 'package:bloc_lecture_practice/counter_bloc/counter_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounterBlocView extends StatelessWidget {
  const CounterBlocView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<CounterBloc, int>(
      builder: (context, state) {
        return Scaffold(
          appBar: AppBar(title: const Text('Counter')),
          body: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('int a: $state', style: TextStyle(fontSize: 40.0)),
              const Divider(thickness: 5),
            ],
          )),
          floatingActionButton: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                  icon: Icon(Icons.add),
                  iconSize: 30,
                  onPressed: () {
                    return context
                        .read<CounterBloc>()
                        .add(CounterEvent.increment);
                  }),
              IconButton(
                  icon: Icon(Icons.remove),
                  iconSize: 30,
                  onPressed: () {
                    return context
                        .read<CounterBloc>()
                        .add(CounterEvent.decrement);
                  }),
              IconButton(
                  icon: Icon(Icons.exposure_zero),
                  iconSize: 30,
                  onPressed: () {
                    return context.read<CounterBloc>().add(CounterEvent.zero);
                  }),
            ],
          ),
        );
      },
    );
  }
}
