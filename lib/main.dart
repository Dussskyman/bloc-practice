import 'package:bloc_lecture_practice/counter/view/counter_page.dart';
import 'package:bloc_lecture_practice/counter_bloc/counter_bloc_view/counter_bloc_page.dart';
import 'package:bloc_lecture_practice/counter_observer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:geolocator/geolocator.dart';
import 'package:permission_handler/permission_handler.dart';

void main() {
  Bloc.observer = CounterObserver();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: CounterBlocPage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    getPosition();
  }

  Stream<Position> getPosition() async* {
    if (await Permission.location.request().isGranted) {
      yield await GeolocatorPlatform.instance.getCurrentPosition();
    } else
      yield null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
          stream: getPosition(),
          builder: (context, AsyncSnapshot<Position> snapshot) {
            if (snapshot.hasData) {
              return Center(
                child: Text(
                    "Longitude: ${snapshot.data.longitude.toString()}\n Latitude: ${snapshot.data.latitude.toString()}"),
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          }),
    );
  }
}
