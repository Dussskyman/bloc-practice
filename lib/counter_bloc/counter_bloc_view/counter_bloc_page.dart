import 'package:bloc_lecture_practice/counter_bloc/counter_bloc.dart';
import 'package:bloc_lecture_practice/counter_bloc/counter_bloc_view/counter_bloc_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CounterBlocPage extends StatelessWidget {
  const CounterBlocPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => CounterBloc(),
      child: CounterBlocView(),
    );
  }
}
