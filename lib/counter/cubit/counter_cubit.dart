import 'package:flutter_bloc/flutter_bloc.dart';

class CounterCubit extends Cubit<Data> {
  CounterCubit() : super(Data(0, 0));
  void increment() => emit(Data(state.i + 1, -state.i - 1));
  void decrement() => emit(Data(state.i - 1, -state.i + 1));
  void zero() => emit(Data(0, 0));
}

class Data {
  final int i;
  final int b;

  Data(this.i, this.b);
}
